FROM rust:centos:7

WORKDIR /opt/api

RUN yum install -y python3>=3.7 python3-pip
RUN pip3 install flask flask_restful

COPY python-api.py /opt/api/python-api.py

CMD ["python3", "/opt/api/python-api.py"]
